# Creedo image

Currently this is an empty repository which is used to store the docker image of creedo application.


## Building the image (legacy)

Note: Currently the build process fails because the base image (`mysql:5.6`) uses a newer version of Debian and the java8 installer tool has been deprecated.
```bash
docker build -t labdev-nomad.esc.rzg.mpg.de:5000/nomadlab/analytics-creedo-archive:v0.4.2-2017-09-29 docker-image
```

## Starting a single user container on the nomad server:
```bash
docker run --name=creedo --restart=unless-stopped -d -p 8805:8080 -v /nomad/nomadlab/beaker-notebooks/test/dataCreedo:/Creedo/persistent_data -v /nomad/nomadlab/beaker-notebooks/test/inboxCreedo:/Creedo/inbox ankitkariryaa/creedo:0.3
```

## Using the docker image from the registry

1. Install docker on your machine
2. Login to the image repository
    ```bash
    docker login gitlab-registry.mpcdf.mpg.de 
    ```
3. Pull the image:
   ```bash
   docker pull gitlab-registry.mpcdf.mpg.de/nomad-lab/analytics-creedo-archive:v0.4.2-2017-09-29 
   ```
   
4. Create a container:
   ```bash
   docker run -p 8080:8080 \
              gitlab-registry.mpcdf.mpg.de/nomad-lab/analytics-creedo-archive:v0.4.2-2017-09-29
   ```
5. Visit: http://localhost:8080/Creedo

Note: The latest tag can be find on the following page:
https://gitlab.mpcdf.mpg.de/nomad-lab/analytics-creedo-archive/container_registry

